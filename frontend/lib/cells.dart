import 'package:flutter/material.dart';
import 'package:collection/collection.dart';
import 'package:animations/animations.dart';
import 'dart:math';
import 'apps_view.dart';


final apps = [
  App('aple', 'dejbdhje djebdhje dedjhebdhe dejd'),
  App('aple', 'dejbdejd'),
  App('ap', 'dejbdhje jkfnkrjfjkrnfkrnjkfjnrdjebdhje dedjhebdhe dejd'),
  App('aplfe', 'dejbdhje djebdhje dedjhebdhe dejd'),
  App('fkrnjfaple', 'dejbdhje djebdhje dedjhebdhe dejd'),
];

class InfoCell extends StatelessWidget {
  final bool normalise;
  final int row;
  final int column;
  final double color_val;
  final List<List<int>> data;
  final List<(String, int)> headers;
  const InfoCell(
      {super.key,
      required this.normalise,
      required this.row,
      required this.column,
      required this.data,
      required this.headers,
      required this.color_val});
  @override
  Widget build(BuildContext context) {
    final color = HSVColor.fromAHSV(1, 0, pow(color_val, 0.3).toDouble(), 0.95);
    final colorFg = color.withValue(0.5).toColor();
    final colorBg = color.toColor();

    final text = normalise?"${(100*data[row][column]/data[row].sum).round().toString()}%":data[row][column] > 999999
        ? data[row][column].toStringAsExponential(4)
        : data[row][column].toString();

    return Expanded(
        child: OpenContainer(
      closedBuilder: (_, openContainer) {
        return InkWell(
            onTap: () => {openContainer()},
            child: Padding(
                padding: const EdgeInsets.all(10),
                child: Center(
                    child: FittedBox(
                        fit: BoxFit.scaleDown,
                        child: Text(text,
                            textScaleFactor: 1.5,
                            style: TextStyle(
                                color: colorFg,
                                fontWeight: FontWeight.w900,
                                fontSize: 45))))));
      },
      openBuilder: (_, closeContainer) {
        return AppsView(sdka: headers[row], sdkb: headers[column], count: data[row][column], colorBg: colorBg, colorFg: colorFg,);
      },
      closedColor: colorBg,
      openColor: colorBg,
      middleColor: colorBg,
      closedElevation: 0,
      openElevation: 0,
      transitionType: ContainerTransitionType.fadeThrough,
      closedShape:
          RoundedRectangleBorder(borderRadius: BorderRadius.circular(0)),
    ));
  }

}

class HeaderCell extends StatelessWidget {
  final List<String> header;
  const HeaderCell({super.key, required this.header});
  @override
  Widget build(BuildContext context) {
    return Expanded(
        child: Container(
            padding: const EdgeInsets.all(10),
            color: Colors.grey[100],
            child: Center(
                child: FittedBox(
                    fit: BoxFit.scaleDown,
                    child: Column(
                        children: header
                            .map((text) => Text(text,
                                style: const TextStyle(
                                    color: Colors.black,
                                    fontWeight: FontWeight.w500,
                                    fontSize: 20)))
                            .toList())))));
  }
}
