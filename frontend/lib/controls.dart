import 'package:flutter/material.dart';
import 'package:textfield_tags/textfield_tags.dart';

class Controls extends StatefulWidget {
  final void Function(List<String> items,bool,bool) onTappedCallback;
  const Controls(
      {super.key, required this.onTappedCallback});

  @override
  State<Controls> createState() => _Controls();
}

class _Controls extends State<Controls> {
  final TextfieldTagsController _controller = TextfieldTagsController();
  bool reload = false;
  bool normalise = false;
  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: const BoxDecoration(
            boxShadow: [
              BoxShadow(
                color: Colors.grey,
                blurRadius: 10,
                offset: Offset(3, 3),
              )
            ],
            color: Colors.white,
            borderRadius: BorderRadius.all(Radius.circular(15))),
        margin: const EdgeInsets.only(left: 30),
        padding: const EdgeInsets.all(10),
        width: 500,
        child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: [
              Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                const Text("Normalize Data"),
                Switch(
                  activeColor: Colors.red[500],
                  activeTrackColor: Colors.red[200],
                    value: normalise,
                    onChanged: (bool value) {
                      setState(() {
                        normalise = value;
                      });
                    }),
              ]),
              TextFieldTags(
                textfieldTagsController: _controller,
                textSeparators: const [','],
                letterCase: LetterCase.normal,
                validator: (String tag) {
                  if (_controller.getTags?.contains(tag) ?? false) {
                    return 'Duplicate SDK name!';
                  } else if ((_controller.getTags?.length ?? 0) > 10) {
                    return 'Please select upto 10 SDKs!';
                  }
                  return null;
                },
                inputfieldBuilder:
                    (context, tec, fn, error, onChanged, onSubmitted) {
                  return ((context, sc, tags, onTagDelete) {
                    return Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: TextField(
                        cursorColor: Colors.red,
                        controller: tec,
                        focusNode: fn,
                        decoration: InputDecoration(
                          hintText: "Enter SDKs. Hit \",\" after each one",
                          isDense: true,
                          border: const OutlineInputBorder(
                            borderSide: BorderSide(
                              width: 3.0,
                            ),
                          ),focusedBorder: const OutlineInputBorder(
                          borderSide: BorderSide(
                            color: Colors.red,
                            width: 3.0,
                          ),
                        ),
                          errorText: error,
                          prefixIconConstraints:
                              const BoxConstraints(maxWidth: 350),
                          prefixIcon: tags.isNotEmpty
                              ? SingleChildScrollView(
                                  controller: sc,
                                  scrollDirection: Axis.horizontal,
                                  child: Row(
                                      children: tags.map((String tag) {
                                    return Container(
                                      decoration: BoxDecoration(
                                        borderRadius: const BorderRadius.all(
                                          Radius.circular(20.0),
                                        ),
                                        color: Colors.red[500],
                                      ),
                                      margin: const EdgeInsets.symmetric(
                                          horizontal: 5.0),
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 10.0, vertical: 5.0),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                            '#$tag',
                                            style: const TextStyle(
                                                color: Colors.white),
                                          ),
                                          const SizedBox(width: 4.0),
                                          InkWell(
                                            child: const Icon(
                                              Icons.cancel,
                                              size: 14.0,
                                              color: Color.fromARGB(
                                                  255, 233, 233, 233),
                                            ),
                                            onTap: () {
                                              onTagDelete(tag);
                                            },
                                          )
                                        ],
                                      ),
                                    );
                                  }).toList()),
                                )
                              : null,
                        ),
                        onChanged: onChanged,
                        onSubmitted: onSubmitted,
                      ),
                    );
                  });
                },
              ),
              const SizedBox(height: 10),
              Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                const Text("Auto-reload"),
                Switch(
                  activeColor: Colors.red[500],
                  activeTrackColor: Colors.red[200],
                    value: reload,
                    onChanged: (bool value) {
                      setState(() {
                        reload = value;
                      });
                    }),
                const SizedBox(width: 100),
                ElevatedButton(
                  style: ElevatedButton.styleFrom(
    backgroundColor: Colors.red
  ),
                    onPressed: () {
                      widget.onTappedCallback(_controller.getTags??[],normalise, reload);
                    },
                    child: const Text('Go'))
              ])
            ]));
  }
}
