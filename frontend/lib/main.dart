import 'package:flutter/material.dart';
import 'controls.dart';
import 'cells.dart';
import 'package:collection/collection.dart';
import 'package:skeletonizer/skeletonizer.dart';
import 'package:http/http.dart' as http;
import 'package:fluttertoast/fluttertoast.dart';
import 'dart:convert';


void main() => runApp(const MainApp());

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: Scaffold(
        body: Main(),
      ),
    );
  }
}

class Main extends StatefulWidget {
  const Main({super.key});

  @override
  State<Main> createState() => _Main();
}

class _Main extends State<Main> {
  List<List<int>> data = [];
  List<(String, int)> headers = [];
  bool loading = false;
  bool normalised = false;

  @override
  Widget build(BuildContext context) {
    return Row(children: [
      Controls(
        onTappedCallback: (items,normalise,reload) async {
          if (items.isEmpty) {
Fluttertoast.showToast(
        msg: "No SDKs selected!",
        toastLength: Toast.LENGTH_SHORT,
        webBgColor: "#cc5555",
webPosition: "center",
        textColor: Colors.white,
        fontSize: 15.0
    );
            return;
          }
          setState(() {
            loading = true;
          });
          try {
            await fetchMatrixData(items);
          } catch(_){
Fluttertoast.showToast(
        msg: "Failed to fetch data!",
        toastLength: Toast.LENGTH_SHORT,
        webBgColor: "#cc5555",
webPosition: "center",
        textColor: Colors.white,
        fontSize: 15.0
    );
          }
          if (headers.isEmpty) {
Fluttertoast.showToast(
        msg: "No SDKs were found in the database!",
        toastLength: Toast.LENGTH_SHORT,
        webBgColor: "#cc5555",
webPosition: "center",
        textColor: Colors.white,
        fontSize: 15.0
    );
          }
          setState(() {
            loading = false;normalised=normalise;
          });
        },
      ),
      Expanded(
          child: Container(
              decoration: const BoxDecoration(
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey,
                      blurRadius: 10,
                      offset: Offset(3, 3),
                    )
                  ],
                  color: Colors.white,
                  borderRadius: BorderRadius.all(Radius.circular(15))),
              margin: const EdgeInsets.all(30),
              padding: const EdgeInsets.all(20),
              child: Container(
                  clipBehavior: Clip.hardEdge,
                  decoration: const BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(15))),
                  child: Center(
                      child: loading
                          ? Skeletonizer(child: matrixSkeletonWidget())
                          : (headers.isNotEmpty
                              ? Column(
                                  crossAxisAlignment:
                                      CrossAxisAlignment.stretch,
                                  children: populateMatrix())
                              : const Center(
                                  child: Text("No data loaded!",
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontWeight: FontWeight.w500,
                                          fontSize: 20))))))))
    ]);
  }

  fetchMatrixData(List<String> items) async {
    final response =
        await http.post(Uri.parse('http://127.0.0.1:5000/matrix_data'),headers: {"Content-Type": "application/json"},body: jsonEncode(items)).timeout(
  const Duration(seconds: 10),
  onTimeout: () {
    return http.Response('Error', 408);
  },
);
    List<dynamic> list = json.decode(response.body);
    List<String> sdks = list[0].cast<String>();
    List<int> ids = list[1].cast<int>();
    headers = [for (int i = 0; i < sdks.length; i++) (sdks[i], ids[i])];
    List<List<dynamic>> temp = list[2].cast<List<dynamic>>();
    data = temp.map((li)=>li.cast<int>()).toList();
  }


  List<Widget> populateMatrix() {
    final infocells = [
      for (int i = 0; i < headers.length; i++)
        Expanded(
            child:
                Row(crossAxisAlignment: CrossAxisAlignment.stretch, children: [
          HeaderCell(header: [headers[i].$1]),
          ...[
            for (int j = 0; j < headers.length; j++)
              InfoCell(
                  normalise: normalised,
                  row: i,
                  column: j,
                  data: data,
                  headers: headers,
                  color_val: data[i][j] / data.map((x) => x.sum).sum)
          ]
        ]))
    ];
    final Widget headercells = Expanded(
        child: Row(crossAxisAlignment: CrossAxisAlignment.stretch, children: [
      const HeaderCell(header: ["To SDK →", "From SDK ↓"]),
      ...headers.map((header) => HeaderCell(header: [header.$1])).toList()
    ]));
    return [headercells, ...infocells];
  }

  Widget matrixSkeletonWidget() {
    return Column(crossAxisAlignment: CrossAxisAlignment.stretch, children: [
      for (int i = 0; i < 5; i++)
        Expanded(
            child:
                Row(crossAxisAlignment: CrossAxisAlignment.stretch, children: [
          for (int i = 0; i < 5; i++)
            Expanded(
                child: Card(
                    color: Colors.grey[100],
                    child: const Center(
                        child: FittedBox(
                            fit: BoxFit.scaleDown,
                            child: Text("dekjn",
                                textScaleFactor: 1.5,
                                style: TextStyle(fontSize: 45)))))),
        ])),
    ]);
  }
}
