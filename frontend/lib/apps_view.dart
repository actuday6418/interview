import 'package:flutter/material.dart';
import 'package:skeletonizer/skeletonizer.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class App {
  final String name;
  final String description;
  App(this.name, this.description);
}

class AppsView extends StatefulWidget {
  final (String,int) sdka;
  final (String,int) sdkb;
  final int count;
  final Color colorBg;
  final Color colorFg;
  const AppsView(
      {super.key, required this.sdka, required this.sdkb, required this.count, required this.colorBg, required this.colorFg});

  @override
  State<AppsView> createState() => _AppsView();
}

class _AppsView extends State<AppsView> {
  bool loading = true;
  List<App> apps = [];

@override
void initState() {
    super.initState();
          ()async {try {
    await fetchData(widget.sdka.$2, widget.sdka.$2==widget.sdkb.$2?null:widget.sdkb.$2);
          } catch(_){
Fluttertoast.showToast(
        msg: "Failed to fetch data!",
        toastLength: Toast.LENGTH_SHORT,
        webBgColor: "#cc5555",
webPosition: "center",
        textColor: Colors.white,
        fontSize: 15.0
    );
          }
          }().then((_)=>setState(() {loading=false;}));
}
  @override
  Widget build(BuildContext context) {
    return Container(
            color: widget.colorBg,
            child: Center(
                child: Container(
                    width: 600,
                    decoration: const BoxDecoration(
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey,
                            blurRadius: 10,
                            offset: Offset(3, 3),
                          )
                        ],
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(15))),
                    padding: const EdgeInsets.all(30),
                    child: Column(mainAxisSize: MainAxisSize.min, children: [
                      FittedBox(
                          fit: BoxFit.scaleDown,
                          child: Text(
                              widget.sdka.$2 == widget.sdkb.$2
                                  ? widget.sdka.$1
                                  : '${widget.sdka.$1} → ${widget.sdkb.$1}',
                              textScaleFactor: 1.5,
                              style: TextStyle(
                                  color: widget.colorFg,
                                  fontWeight: FontWeight.w700,
                                  fontSize: 45))),
                      const SizedBox(height: 10.0),
                      Text(
                              widget.sdka.$2 == widget.sdkb.$2
                              ? '${widget.count} apps use ${widget.sdka.$1}'
                              : '${widget.count} ${widget.count == 1 ? "app" : "apps"} transitioned from ${widget.sdka.$1} to ${widget.sdka.$1}',
                          style: const TextStyle(
                              color: Colors.grey, fontSize: 20)),
                      const SizedBox(height: 30.0),
                      SizedBox(
                          height: 450,
                          child: Scrollbar(
                              thumbVisibility: true,
                              child: (){
                                final data = loading?[for(int i=0;i<10;i++) App("dekjnde", "1234-32-43")]:apps; 
                                return Skeletonizer(
enabled: loading,
                                  child: ListView.builder(
                                  itemCount: data.length,
                                  itemBuilder: (context, i) => Container(
                                        padding: const EdgeInsets.all(20),
                                        margin: const EdgeInsets.all(20),
                                        decoration: const BoxDecoration(
                                            boxShadow: [
                                              BoxShadow(
                                                color: Colors.grey,
                                                blurRadius: 5,
                                                offset: Offset(2, 2),
                                              )
                                            ],
                                            color: Colors.white,
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(15))),
                                        child: Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                          Text(data[i].name,
                                              style: TextStyle(
                                                  color: widget.colorFg,
                                                  fontWeight: FontWeight.w700,
                                                  fontSize: 35)),
                      const SizedBox(height: 10.0),
                                          Text(data[i].description,
                                              style: const TextStyle(
                                                  color: Colors.grey,
                                                  fontSize: 20))
                                        ]),
                                      )));}()
                                          )),
                      const SizedBox(height: 30.0),
                      ElevatedButton(
                        onPressed: () => Navigator.pop(context),
                        style: ElevatedButton.styleFrom(
    backgroundColor: widget.colorFg,
  ),
                        child: const Text('Go back'),
                      )
                    ]))));
  }


  fetchData(int sdka, int? sdkb) async {

    final response = sdkb == null?
    
        await http.get(Uri.parse('http://127.0.0.1:5000/apps_using_sdk/$sdka')):
await http.get(Uri.parse('http://127.0.0.1:5000/apps_from_to_sdk/$sdka/$sdkb'));
    List<dynamic> li = json.decode(response.body);
    apps = li.map((e)=>App(e[0].toString(),e[1].toString())).toList();
  }
}
