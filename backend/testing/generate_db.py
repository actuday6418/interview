import string
import random
import sqlite3
from typing import List


class App:
    def __init__(self, id: int):
        self.name = "".join([random.choice(string.ascii_letters)])
        self.release_date = "".join([random.choice(string.ascii_letters)])
        self.id = id


class Sdk:
    def __init__(self, id: int):
        self.name = "".join([random.choice(string.ascii_letters)])
        self.id = id


class AppSdk:
    def __init__(self, app: int, sdk: int, installed: bool):
        self.app = app
        self.sdk = sdk
        self.installed = installed


def getData(object) -> List:
    return list(vars(object).values())


apps = [App(id) for id in range(10000000)]
print("Generated apps")
sdks = [Sdk(id) for id in range(100000)]
print("Generated SDKs")
app_sdks = []
for app in apps:
    for _ in range(100):
        sdk = random.choice(sdks)
        if not any([e.app == app.id and e.sdk == sdk.id for e in app_sdks]):
            app_sdks.append(AppSdk(app.id, sdk.id, random.choice([True, False])))
print("Generated app SDKs")

cursor = sqlite3.connect("test.db").cursor()
cursor.executemany(
    "insert into app values (?,?,?)",
    list(map(apps, getData)),
)
