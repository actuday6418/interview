use indicatif::ProgressBar;
use rand::seq::SliceRandom;
use rand::{distributions::Alphanumeric, Rng};
use rayon::prelude::*;
use rusqlite::Connection;

fn random_string() -> String {
    rand::thread_rng()
        .sample_iter(&Alphanumeric)
        .take(7)
        .map(char::from)
        .collect()
}

fn gen_app(id: i32) -> (i32, String, String) {
    (id, random_string(), random_string())
}

fn gen_sdk(id: i32) -> (i32, String) {
    (id, random_string())
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let mut conn = Connection::open("test.db")?;
    conn.execute_batch(
        "CREATE TABLE app(id int primary key, name text, release_date text);
         CREATE TABLE sdk(id int primary key, name text);
         CREATE TABLE app_sdk(app_id int references app(id), sdk_id int references sdk(id), installed boolean, primary key (app_id, sdk_id));
         ",
    )?;
    let tx = conn.transaction()?;
    let apps: Vec<(i32, String, String)> = (0..10_000_000).into_par_iter().map(gen_app).collect();
    let sdks: Vec<(i32, String)> = (0..100_000).map(gen_sdk).collect();
    let mut stmt = tx.prepare("INSERT INTO app VALUES (?1, ?2, ?3)")?;
    for app in &apps {
        stmt.execute(app.clone())?;
    }
    println!("Inserted apps");
    stmt = tx.prepare("INSERT INTO sdk VALUES (?1, ?2)")?;
    for sdk in &sdks {
        stmt.execute(sdk.clone())?;
    }
    println!("Inserted SDKs");
    stmt = tx.prepare("INSERT INTO app_sdk VALUES (?1, ?2, ?3)")?;
    let pb = ProgressBar::new(100);
    let mut counter = 0;
    apps.iter().for_each(|app| {
        let mut app_sdks: Vec<(i32, i32, bool)> = Vec::with_capacity(100);
        for _ in 0..100 {
            let sdk = sdks.choose(&mut rand::thread_rng()).unwrap();
            if app_sdks.iter().find(|e| e.1 == sdk.0).is_none() {
                app_sdks.push((app.0, sdk.0, rand::thread_rng().gen_bool(0.5)));
            }
        }
        for app_sdk in app_sdks {
            stmt.execute(app_sdk).unwrap();
        }
        counter += 1;
        if counter % 100_000 == 0 {
            pb.set_position(counter / 100_000);
        }
    });
    println!("Inserted app SDKs. Creating Index..");

    drop(stmt);
    tx.commit()?;
    conn.execute_batch("create index sdk_installed on app_sdk(sdk_id,installed);")?;
    Ok(())
}
