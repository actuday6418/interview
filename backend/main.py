from typing import List
from flask import Flask, request, g, jsonify, send_from_directory
import sqlite3

app = Flask("Data Matrix Backend")


def get_db():
    db = getattr(g, "_database", None)
    if db is None:
        db = g._database = sqlite3.connect("data.db")
    return db


@app.teardown_appcontext
def close_connection(_):
    db = getattr(g, "_database", None)
    if db is not None:
        db.close()

@app.route("/<path:path>", methods=["GET"])
def index(path):
    return send_from_directory("static",path)

@app.route("/apps_from_to_sdk/<from_sdk>/<to_sdk>", methods=["GET"])
def apps_from_to_sdk(from_sdk, to_sdk):
    cursor = get_db().cursor()
    return jsonify(
        cursor.execute(
            "select name, release_date from app where id in (select app_id from app_sdk where sdk_id=? and installed=0 and app_id in (select app_id from app_sdk where sdk_id=? and installed=1));",
            [from_sdk, to_sdk],
        ).fetchall()
    )


@app.route("/apps_using_sdk/<sdk>", methods=["GET"])
def apps_using_sdk(sdk):
    cursor = get_db().cursor()
    return jsonify(
        cursor.execute(
            "select name, release_date from app where id in (select app_id from app_sdk where sdk_id=? and installed=1);",
            [sdk],
        ).fetchall()
    )


# Expects a JSON list of SDK names to generate data for
@app.route("/matrix_data", methods=["POST"])
def matrix_data():
    cursor = get_db().cursor()
    sdks: List[str] = request.json

    # get SDKs present in DB
    # sanitise sdks
    query = f"select id,name from sdk where name in ({','.join(['?']*len(sdks))})"
    results = cursor.execute(query, sdks).fetchall()
    valid_sdk_ids = [id for id, _ in results]
    valid_sdk_names = [name for _, name in results]

    # populate matrix
    matrix = []
    for i in valid_sdk_ids:
        row = []
        for j in valid_sdk_ids:
            if i == j:
                result = [
                    i
                    for i, in cursor.execute(
                        "select count(app_id) from app_sdk where sdk_id=? and installed=1;",
                        [i],
                    ).fetchall()
                ][0]
            else:
                result = [
                    i
                    for i, in cursor.execute(
                        "select count(app_id) from app_sdk where sdk_id=? and installed=0 and app_id in (select app_id from app_sdk where sdk_id=? and installed=1);",
                        [i, j],
                    ).fetchall()
                ][0]
            row.append(result)
        matrix.append(row)

    return jsonify(valid_sdk_names, valid_sdk_ids, matrix)


app.run(debug=True)
